import warnings
warnings.filterwarnings(action='ignore')

import glob
import os
import random

import torch
import torch.onnx
import torch.optim
from torch import nn
from torch.utils.data import DataLoader
from torch.optim import Adam

# import torchvision
from torchvision.transforms import transforms
from torchvision import transforms, torch

from pandas.core.common import flatten
import numpy as np

import matplotlib.pyplot as plt
from PIL import Image, UnidentifiedImageError

from FormulaDataset import FormulaDataset
     
# CUDA & TORCH CONFIG
os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ["TORCH_USE_CUDA_DSA"] = '1'

#TF config
os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'

# define
SEED = "GEORGE_RUSSELL_63"
EPOCH_CNT = 150
BATCH_SIZE = 8
TEST_SORCE_PER = 0.4
IMG_SOURCE_SIZE = 192

root = './data' 
dirs = os.listdir(root)

# define Label TYPES
classes = ('amr', 'ferarri', 'mcl', 'merc', 'others', 'rbr', 'vehicle', 'williams')


def validate_image(filepath):
    try:
        img = Image.open(filepath).convert('RGB')
        img.load()

    except UnidentifiedImageError:
        print(f'Corrupted Image is found at: { filepath }')
        return False
    
    except (IOError, OSError):
        print(f'Truncated Image is found at: { filepath }')
        return False
    else:
        return True


for dir_ in dirs:
    folder_path = os.path.join(root, dir_)
    files = os.listdir(folder_path)
    
    images = [os.path.join(folder_path, f) for f in files]
    for img in images:
        valid = validate_image(img)
        if not valid:
            os.remove(img)

folders = glob.glob('data/*')

train_images = []
test_images = []

for folder in folders:
    label = os.path.basename(folder)
    files = sorted(glob.glob(folder + '/*'))

    random.seed(SEED)
    random.shuffle(files)

    idx = int(len(files) * TEST_SORCE_PER)
    train = files[:-idx]
    test = files[-idx:]

    train_images.extend(train)
    test_images.extend(test)

classToindex = {os.path.basename(f):idx for idx, f in enumerate(folders)}

# Labels
train_labels = [f.split('\\')[1] for f in train_images]
test_labels = [f.split('\\')[1] for f in test_images]

# print('===' * 10)
# print(f'train images: {len(train_images)}')
# print(f'train labels: {len(train_labels)}')
# print(f'test images: {len(test_images)}')
# print(f'test labels: {len(test_labels)}')

train_transform = transforms.Compose([
    transforms.Resize((IMG_SOURCE_SIZE, IMG_SOURCE_SIZE)),          
    # transforms.ColorJitter(brightness = 0.1, contrast = 0.1, saturation = 0.1, hue = 0.1),
    transforms.RandomHorizontalFlip(p = 0.2),
    transforms.RandomVerticalFlip(p = 0.2),
    transforms.ToTensor(), 
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    # transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
])

test_transform = transforms.Compose([
    transforms.Resize((IMG_SOURCE_SIZE, IMG_SOURCE_SIZE)), 
    transforms.ToTensor(), 
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    # transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)) 
])

train_dataset = FormulaDataset(train_images, train_labels, classToindex, train_transform)
test_dataset = FormulaDataset(test_images, test_labels, classToindex, test_transform)


train_loader = DataLoader(train_dataset, 
                          batch_size=BATCH_SIZE, 
                          shuffle=True,
                          num_workers=0)

test_loader = DataLoader(test_dataset, 
                         batch_size=BATCH_SIZE, 
                         shuffle=True,
                         num_workers=2)


# Define a convolution neural network
class Network(nn.Module):

    def __init__(self):
        super(Network, self).__init__()
        self.layer1 = torch.nn.Sequential(
            nn.Conv2d(3, 8, kernel_size=3, stride=1, padding=1), #cnn layer
            nn.ReLU(), #activation function
            nn.MaxPool2d(kernel_size=2, stride=2)) #pooling layer
        
        self.layer2 = torch.nn.Sequential(
            nn.Conv2d(8, 16, kernel_size=3, stride=1, padding=1), #cnn layer
            nn.ReLU(), #activation function
            nn.MaxPool2d(kernel_size=2, stride=2)) #pooling layer
        
        self.layer3 = torch.nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1), #cnn layer
            nn.ReLU(), #activation function
            nn.MaxPool2d(kernel_size=2, stride=2)) #pooling layer
        
        self.layer4 = torch.nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=4, stride=1, padding=1), #cnn layer
            nn.ReLU(), #activation function
            nn.MaxPool2d(kernel_size=2, stride=2)) #pooling layer
        
        self.fc_layer = nn.Sequential( 
            nn.Linear(7744, 11) #fully connected layer(ouput layer)
        )    
        
    def forward(self, x):
        x = self.layer1(x) 
        x = self.layer2(x) 
        x = self.layer3(x) 
        x = self.layer4(x) 
        x = torch.flatten(x, start_dim=1)
        
        out = self.fc_layer(x)
        return out

# Instantiate a neural network model 
model = Network()

loss_fn = nn.CrossEntropyLoss()
optimizer = Adam(model.parameters(), lr=0.001, weight_decay=0.0001)

from torch.autograd import Variable

# Function to save the model
def saveModel():
    path = "./FormulaModel.pth"
    torch.save(model.state_dict(), path)

# Function to test the model with the test dataset and print the accuracy for the test images
# FIXME error occur!    
def testAccuracy():
    model.eval()
    accuracy = 0.0
    total = 0.0
    
    with torch.no_grad():
        for data in test_loader:
            images, labels = data
           
            # run the model on the test set to predict labels
            images = Variable(images.to("cuda:0"))
            labels = Variable(labels.to("cuda:0"))
            outputs = model(images)

            # the label with the highest energy will be our prediction
            _, predicted = torch.max(outputs.data, dim=1)
            total += labels.size(0)
            accuracy += (predicted == labels).sum().item()
    
    # compute the accuracy over all test images
    accuracy = (100 * accuracy / total)
    return(accuracy)


# Training function. We simply have to loop over our data iterator and feed the inputs to the network and optimize.
def train(num_epochs):
    best_accuracy = 0.0

    # Define your execution device
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # print("The model will be running on", device, "device")
    # Convert model parameters and buffers to CPU or Cuda
    model.to(device)

    for epoch in range(num_epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        # running_acc = 0.0

        for i, (images, labels) in enumerate(train_loader, 0):
            
            # get the inputs
            images = Variable(images.to(device))
            labels = Variable(labels.to(device))


            # predict classes using images from the training set
            outputs = model(images)
            # zero the parameter gradients
            optimizer.zero_grad()
            # compute the loss based on model output and real labels
            loss = loss_fn(outputs, labels)
            # backpropagate the loss
            loss.backward()
            # adjust parameters based on the calculated gradients
            optimizer.step()

            # Let's print statistics for every 1,000 images
            running_loss += loss.item()     # extract the loss value
            if i % 1000 == 999:    
                # print every 1000 (twice per epoch) 
                print('[%d, %5d] loss: %.3f' %  (epoch + 1, i +1, running_loss / 1000))
                # zero the loss
                running_loss = 0.0

        # Compute and print the average accuracy fo this epoch when tested over all 10000 test images
        accuracy = testAccuracy()
        print('For epoch', epoch +1,'the test accuracy over the whole test set is %d %%' % (accuracy))
        # we want to save the model if the accuracy is the best
        if accuracy > best_accuracy:
            saveModel()
            best_accuracy = accuracy
            
# Function to show the images
def imageshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


# Function to test the model with a batch of images and show the labels predictions
def testBatch():
    # get batch of images from the test DataLoader  
    images, labels = next(iter(test_loader))

    # show all images as one image grid
    # imageshow(torchvision.utils.make_grid(images))
   
    # Show the real labels on the screen 
    print('Real labels: ', ' '.join('%5s' % classes[labels[j]] 
                               for j in range(BATCH_SIZE)))
  
    # Let's see what if the model identifiers the  labels of those example
    outputs = model(images)
    
    # We got the probability for every 10 labels. The highest (max) probability should be correct label
    _, predicted = torch.max(outputs, dim=1)
    
    # Let's show the predicted labels on the screen to compare with the real ones
    print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] 
                                   for j in range(BATCH_SIZE)))
    
#Function to Convert to ONNX 
def convert_ONNX(): 
    # set the model to inference mode 
    model.eval() 

    # Let's create a dummy input tensor  
    dummy_input = torch.empty(1, 3, IMG_SOURCE_SIZE, IMG_SOURCE_SIZE, dtype=torch.float32)  
    # dummy_input = torch.randn(8, 3, 3, 3, requires_grad=True)

    # Export the model   
    torch.onnx.export(model,         # model being run 
         dummy_input,       # model input (or a tuple for multiple inputs) 
         "FormulaModel.onnx",       # where to save the model  
        #  export_params=True,  # store the trained parameter weights inside the model file 
        #  opset_version=10,    # the ONNX version to export the model to 
        #  do_constant_folding=True,  # whether to execute constant folding for optimization 
         input_names = ['modelInput'],   # the model's input names 
         output_names = ['modelOutput'], # the model's output names 
        #  dynamic_axes={'modelInput' : {0 : 'batch_size'},    # variable length axes 
                                # 'modelOutput' : {0 : 'batch_size'}}) 
    )
    print(" ") 
    print('Model has been converted to *.ONNX')     


if __name__ == "__main__":
    
    # Let's build our model
    train(EPOCH_CNT)
    print('Finished Training')
    
    # Let's load the model we just created and test the accuracy per label
    model = Network()
    path = "./FormulaModel.pth"
    model.load_state_dict(torch.load(path))

    # Test with batch of images
    testBatch()
    convert_ONNX()