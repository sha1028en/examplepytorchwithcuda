from PIL import Image
import cv2
import numpy as np
import time
import torch
import torchvision
from torch.utils.data import Dataset
from torchvision import transforms
import albumentations
from matplotlib import pyplot as plt

import os
import pandas as pd
from torchvision.io import read_image

class FormulaDataset(Dataset):
    def __init__(self, files, labels, classToIndex, transform=None):
        super(Dataset, self).__init__()

        self.files = files
        self.labels = labels
        self.classToIndex = classToIndex
        self.transform = transform

    def __len__(self):
        return len(self.files)
    
    def __getitem__(self, index):
        file = self.files[index]
        
        image = Image.open(file).convert('RGB')
        image = self.transform(image)
        label = self.classToIndex[self.labels[index]]

        return image, label