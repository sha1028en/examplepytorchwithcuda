package kr.co.citus.exampleml.util.converter

class ClassesLabelConverter private constructor() {
    companion object {

        // label to name
        fun convertLabelToRealName(label: String): String {
            var realName = label

            ClassesDefine.values().forEach {
                if(it.labelName == label) {
                    realName = it.realName
                    return realName
                }
            }
            return realName
        }


        // name to label
        fun convertRealNameToLabel(realName: String): String {
            var label = realName

            ClassesDefine.values().forEach {
                if(it.realName == realName) {
                    label = it.labelName
                    return label
                }
            }
            return label
        }
    }
}