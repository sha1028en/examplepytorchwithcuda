package kr.co.citus.exampleml.dialog.importImage

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import kr.co.citus.exampleml.databinding.DialogImportImgBinding


class ImportImageDialog(context: Context, private val clickListener: ImportImageDialogOnClickListener) : Dialog(context) {
    private val binding: DialogImportImgBinding by lazy { DialogImportImgBinding.inflate(LayoutInflater.from(context)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(this.binding.root)
    }

    fun showDialog() {
        this.show()

        // import image from Gallery
        this.binding.importAlbum.setOnClickListener {
            this.clickListener.onClickAlbum()
            this.dismiss()
        }

        // import image from camera
        this.binding.importCamera.setOnClickListener {
            this.clickListener.onClickCamera()
            this.dismiss()
        }
    }
}