package kr.co.citus.exampleml.util.perm

import android.os.Build
import kr.co.citus.exampleml.util.perm.data.CommonPermUnit
import kr.co.citus.exampleml.util.perm.data.PermFlowControllable
import kr.co.citus.exampleml.util.perm.data.UrgentPermUnit
import kr.co.citus.exampleml.util.perm.data.version.tiramisu.CommonTiramisuPerm
import kr.co.citus.exampleml.util.perm.data.version.nougat.UrgentNougatPerm
import kr.co.citus.exampleml.util.perm.data.version.quincetart.UrgentQuinceTartPerm
import kr.co.citus.exampleml.util.perm.data.version.snowcone.UrgentSnowConePerm
import kr.co.citus.exampleml.util.perm.data.version.tiramisu.UrgentTiramisuPerm
import java.util.LinkedList

/**
 * @author dspark
 * @since 2023-12-12
 *
 * 일반 코드에 버전별로 퍼미션을 만드는게 너무 지저분해 보여 관련 코드를 따로 뺀다
 * static 메서드를 통해 접근해야 하며 별도의 인스턴스 초기화는 필요 없다.
 *
 * 해당 기기의 OS 버전을 확인하여 버전에 맞는 퍼미션 데이터 클래스 목록을 리턴
 *
 * @see PermContractManager
 * @see UrgentTiramisuPerm
 */
class PermDataListConstructor private constructor() {

    companion object {

        /**
         * 안드로이드용 필수 권한 목록을 리턴한다
         *
         * @return 안드로이드 13, 12, 10, 7에 대한 권한을 분류하여 리스트로 리턴
         * @see UrgentPermUnit 필수 퍼미션 데이터 클래스
         */
        fun getUrgentPermList(): List<UrgentPermUnit> {
            val urgentPermList = LinkedList<UrgentPermUnit>()

            // 13 ~ 14
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                UrgentTiramisuPerm.values().forEach {
                    urgentPermList.add(buildUrgentPrem(it.getAndroidPermName(), it.getDisplayMessageResId()))
                }

            // 12
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                UrgentSnowConePerm.values().forEach {
                    urgentPermList.add(buildUrgentPrem(it.getAndroidPermName(), it.getDisplayMessageResId()))
                }

            // 10
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                UrgentQuinceTartPerm.values().forEach {
                    urgentPermList.add(buildUrgentPrem(it.getAndroidPermName(), it.getDisplayMessageResId()))
                }

            // 9 ~ 7
            } else {
                UrgentNougatPerm.values().forEach {
                    urgentPermList.add(buildUrgentPrem(it.getAndroidPermName(), it.getDisplayMessageResId()))
                }
            }
            return urgentPermList
        }

        /**
         * 안드로이드용 선택 권한 목록을 리턴한다
         *
         * @return 안드로이드 13 ~ 에 대한 권한을 분류하여 리스트로 리턴
         * @see CommonPermUnit 선택 퍼미션 데이터 클래스
         */
        fun getCommonPermList(): List<CommonPermUnit> {
            val commonPermList = LinkedList<CommonPermUnit>()

            // 13 ~
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                CommonTiramisuPerm.values().forEach {
                    commonPermList.add(buildCommonPerm(it.getAndroidPermName(), it.getDisplayMessageResId()))
                }
            }
            return commonPermList
        }

        /**
         * 안드로이드용 선택 / 필수 권한 목록을 리턴한다
         *
         * @return List &lt PermFlowContrllable &gt  안드로이드 13, 12, 10, 7에 대한 권한을 분류하여 리턴
         *
         * @see CommonPermUnit 선택 퍼미션 데이터 클래스
         * @see UrgentPermUnit 필수 퍼미션 데이터 클래스
         * @see PermFlowControllable 각 퍼미션 데이터 클래스의 동작 정의
         */
        fun getAllPermList(): List<PermFlowControllable> {
            val permList = LinkedList<PermFlowControllable>()

            permList.addAll(getUrgentPermList())
            permList.addAll(getCommonPermList())

            return permList
        }

        /**
         * @return 필수 권한 데이터 클래스를 생성해서 리턴한다
         * @see UrgentPermUnit 필수 퍼미션 데이터 클래스
         */
        fun buildUrgentPrem(androidPermName: String, toDisplayMessageResId: Int): UrgentPermUnit {
            return UrgentPermUnit(androidPermName, toDisplayMessageResId.toString())
        }

        /**
         * @return 선택 권한 데이터 클래스를 생성해서 리턴
         * @see CommonPermUnit 선택 퍼미션 데이터 클래스
         */
        private fun buildCommonPerm(androidPermName: String, toDisplayMessageResId: Int): CommonPermUnit {
            return CommonPermUnit(androidPermName, toDisplayMessageResId.toString())
        }
    }
}