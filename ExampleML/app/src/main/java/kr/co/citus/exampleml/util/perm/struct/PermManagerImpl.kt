package kr.co.citus.exampleml.util.perm.struct

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kr.co.citus.exampleml.util.perm.data.PermFlowControllable
import java.util.LinkedList
import java.util.stream.Collectors

/**
 * @param activity 안드로이드 액티비티, 퍼미션 검증을 위함
 *
 * onCreate()에서 해당 클래스를 초기화 해야 한다.
 * onStart()이후 초기화 해서 사용하면 앱이 죽을꺼다.
 *
 * 외부에서 날것으로 접근하지 말것!!!
 */
abstract class PermManagerImpl(activity: AppCompatActivity) {
    private var requestPermLauncher: ActivityResultLauncher<Array<String>>?
    private val requestPermList: LinkedList<PermFlowControllable> = LinkedList()

    /**
     * 모든 권한을 허락 받았다면, 분기
     */
    abstract fun onContractAll(reqPermList: LinkedList<PermFlowControllable>)

    /**
     * 권한을 허락받지 못했다면, 해당 권한 정보와 함께 분기
     */
    abstract fun onFailContract(permName: String, toDisplayMessage: String)

    init {
        // 2023-12-11 dspark
        // 퍼미션을 허락 받았는지 확인해 본다

        // 퍼미션을 허락 받으면 onGrant()
        // onGrant는 대체로 무시한다.

        // 퍼미션을 허락 받지 못했다면 onRevoke(): Boolean 으로 분기한다
        // onRevoke()의 리턴값이 true면 퍼미션을 허락받지 못했지만 문제삼지 않고 다음 퍼미션 결과를 확인 ( 선택 권한을 허락받지 못한것으로 간주 )
        // onRevoke()의 리턴값이 false면 필수 퍼미션을 허락받지 못한것으로 간주하고 onFailContract()를 부르고 나머지 처리는 무시한다

        // 전부 허락 받았다면, onContractAll() 을 부르고 종료한다.
        this.requestPermLauncher = activity.registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result ->
            // true면 계속 다음 퍼미션을 검증
            // false면 다음 퍼미션을 검증하지 않고 루프문을 뛰쳐 나온뒤, 검증하던 퍼미션에 대한 실패 콜백을 던진다.
            var hasAllGrant = true

            // 권한별로 for 돌아라
            result.entries.forEach { it ->
                // 허락받지 못한 퍼미션이 있음? 그럼 루프 중단
                if (hasAllGrant) {

                    // 하나의 퍼미션에 대한 정보를 빼낸다
                    val currentPermName = it.key
                    val permUnit: PermFlowControllable? = try {
                        this.requestPermList.stream().filter {
                            it.getPermName() == currentPermName

                        }.findFirst().get()

                    } catch (e: NoSuchElementException) {
                        e.printStackTrace()
                        onFailContract("", "")
                        return@registerForActivityResult
                    }

                    // 그래서 퍼미션 검증 결과는???
                    when {
                        it.value -> {
                            permUnit.let { it?.onGrant() }
                        }

                        ActivityCompat.shouldShowRequestPermissionRationale(activity, it.key) -> {
                            hasAllGrant = permUnit.let { it?.onRevoke() } ?: true

                        } else -> {
                            hasAllGrant = permUnit.let { it?.onRevoke() } ?: true
                        }
                    }

                    if (!hasAllGrant && permUnit != null) {
                        this.onFailContract(permUnit.getPermName(), permUnit.getPermDisplayMessage())
                    }
                }
            }
            if (hasAllGrant) this.onContractAll(this.requestPermList)
        }
    }

    /**
     * 자원을 반납하고 사용 불가 상태로 만든다
     */
    open fun release() {
        this.requestPermLauncher = null
    }

    /**
     * 퍼미션 검증을 시작한다
     * 안드로이드 퍼미션만 검증되며, 커스텀 퍼미션은 필터링되어 검증하지 않을수 있다.
     *
     * @param toVerifyPermList 검증할 "안드로이드" 퍼미션 데이터 클래스 목록들
     * @see kr.or.forest.smartdaslim.util.manager.perm.data.UrgentPermUnit 필수 퍼미션 데이터 클래스
     * @see kr.or.forest.smartdaslim.util.manager.perm.data.CommonPermUnit 선택 퍼미션 데이터 클래스
     */
    fun doVerifyPerm(toVerifyPermList: List<PermFlowControllable>) {
        this.requestPermList.clear()
        this.requestPermList.addAll(toVerifyPermList.toList())

        val buffer: MutableList<String> =
            this.requestPermList.stream().filter {
                it.getPermName().contains("android.permission.")

            }.map(PermFlowControllable::getPermName).collect(Collectors.toList())

        val permNameList = buffer.toTypedArray()

        if (this.requestPermList.isNotEmpty()) {
            this.requestPermLauncher.let { it?.launch(permNameList) }
        }
    }
}