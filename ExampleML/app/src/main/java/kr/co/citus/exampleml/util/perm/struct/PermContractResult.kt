package kr.or.forest.smartdaslim.util.manager.perm.struct

import kr.co.citus.exampleml.util.perm.data.PermFlowControllable
import java.util.LinkedList

/**
 * @author dspark
 * @since 2023-12-11
 *
 * 사용자 퍼미션 허용 / 거절에 대한 콜백 리스너를 정의
 *
 * 선택 퍼미션은 거절해도 onFailContract()를 타지 않는다
 *
 * @see PermManagerImpl
 * @see kr.or.forest.smartdaslim.util.manager.perm.PermContractManager
 */
interface PermContractResult {

    /**
     * 모든 퍼미션을 검증했을때 분기
     */
    fun onContractAll(reqPermList: LinkedList<PermFlowControllable>)

    /**
     * 필수 퍼미션이 거부 당했을때 분기
     *
     * @param permName 거절한 안드로이드 퍼미션 이름
     * @param toDisplayMessage 사용자에게 표시할 안내 문장
     */
    fun onFailContract(permName: String, toDisplayMessage: String)
}