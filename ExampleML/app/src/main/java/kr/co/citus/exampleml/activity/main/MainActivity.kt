package kr.co.citus.exampleml.activity.main

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.mlkit.vision.common.InputImage
import kr.co.citus.exampleml.R
import kr.co.citus.exampleml.activity.camera.CameraCaptureActivity
import kr.co.citus.exampleml.databinding.ActivityMainBinding
import kr.co.citus.exampleml.dialog.importImage.ImportImageDialog
import kr.co.citus.exampleml.dialog.importImage.ImportImageDialogOnClickListener
import kr.co.citus.exampleml.util.converter.ClassesLabelConverter
import kr.co.citus.exampleml.util.perm.PermContractManager
import kr.co.citus.exampleml.util.perm.PermDataListConstructor
import kr.co.citus.exampleml.util.perm.data.PermFlowControllable
import kr.or.forest.smartdaslim.util.manager.perm.struct.PermContractResult
import java.lang.StringBuilder
import java.util.LinkedList

class MainActivity : AppCompatActivity() {
    // Verify Permission
    private lateinit var permManager: PermContractManager

    // Request Img by Gallery
    private lateinit var reqImgLauncherByGallery: ActivityResultLauncher<String>

    // Response Img by Gallery or Camera
    private lateinit var responseImgLauncher: ActivityResultLauncher<Intent>

    // Request Img by Camera
    private val importImageDialog by lazy {
        ImportImageDialog(this, importImgClickListener)
    }

    // Select way how to Request Img ( Cam or Gallery )
    private val importImgClickListener by lazy {
        object : ImportImageDialogOnClickListener {
            override fun onClickAlbum() {
                permManager.doVerifyPerm(listOf<PermFlowControllable>(PermDataListConstructor.buildUrgentPrem(Manifest.permission.READ_MEDIA_IMAGES, 0)))
            }

            override fun onClickCamera() {
                permManager.doVerifyPerm(listOf<PermFlowControllable>(PermDataListConstructor.buildUrgentPrem(Manifest.permission.CAMERA, 0)))
            }
        }
    }

    // ViewModel LiveData Observer
    private val imgObserver by lazy {
        Observer<Bitmap?> {
            it?.let { img ->
                Glide.with(this.binding.imgRoot)
                    .load(img)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .apply(RequestOptions.formatOf(DecodeFormat.PREFER_RGB_565))
                    .into(this.binding.imgRoot)

            }
        }
    }

    // ViewModel LiveData Observer
    private val labelObserver by lazy {
        Observer<String> {
            this.binding.labelText.text = it
        }
    }


    private val binding by lazy {
        ActivityMainBinding.inflate(this.layoutInflater)
    }

    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProvider(this@MainActivity)[MainActivityViewModel::class.java]
    }

    private val logTag = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(this.binding.root)

        // append viewModel observer
        this.viewModel.appendImgObserver(this, this.imgObserver)
        this.viewModel.appendLabelObserver(this, this.labelObserver)

        this.binding.imgRoot.setOnClickListener {
            this.importImageDialog.showDialog()
        }

        // 장난좀 처 볼까?
        this.binding.esterEgg.setOnClickListener {
            if (this.binding.labelText.text == this.getString(R.string.hello_world)) {
                this.viewModel.setImg(BitmapFactory.decodeResource(this@MainActivity.resources, R.drawable.sf01))
                this.viewModel.setLabel("마굿간 수준 wwwwww")
            }
        }

        this.permManager = PermContractManager(this, object : PermContractResult {
            override fun onContractAll(reqPermList: LinkedList<PermFlowControllable>) {
                if (reqPermList.last.getPermName() == Manifest.permission.READ_MEDIA_IMAGES) {
                    reqImgLauncherByGallery.launch(Manifest.permission.READ_MEDIA_IMAGES)

                } else if (reqPermList.last.getPermName() == Manifest.permission.CAMERA) {
                    val intent = Intent(this@MainActivity, CameraCaptureActivity::class.java)
                    responseImgLauncher.launch(intent)
                }
            }

            override fun onFailContract(permName: String, toDisplayMessage: String) {
                Toast.makeText(this@MainActivity.applicationContext, R.string.toast_need_grant_before_perm_use_func, Toast.LENGTH_SHORT).show()
            }
        })

        this.responseImgLauncher = this.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                val imgUri = it.data?.data

                imgUri?.let { uri ->
                    val img = InputImage.fromFilePath(this@MainActivity, uri)

                    img.bitmapInternal?.let { bitmap ->
                        this.viewModel.setImg(bitmap)
                    }

                    this.viewModel.processImageLabel(img,
                        OnSuccessListener { list ->
                            val labels = StringBuilder()
                            list.forEach { imgLabel ->
                                labels.append(ClassesLabelConverter.convertLabelToRealName(imgLabel.text.replace("\r", "")))
                                if (list.last() != imgLabel) {
                                    labels.append(", ")
                                }
                            }

                            if (labels.isBlank()) {
                                labels.append(this.getString(R.string.toast_cant_classification))
                            }

                            Log.d("$logTag ImgLabeler.process result: ", list.toString())
                            this.viewModel.setLabel(labels.toString())
                        },

                        OnFailureListener { e ->
                            e.printStackTrace()
                            Toast.makeText(this@MainActivity, R.string.toast_cant_predicted_model, Toast.LENGTH_SHORT).show()
                    })
                }
            }
        }

        this.reqImgLauncherByGallery = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                val intent = Intent(Intent.ACTION_PICK)
                intent.setDataAndType(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    "image/*"
                )
                responseImgLauncher.launch(intent)

            } else {
                Log.d(logTag, "request img: deny")
            }
        }
    }

    override fun onDestroy() {
        this.permManager.release()

        this.viewModel.dependImgObserver(this.imgObserver)
        this.viewModel.dependLabelObserver(this.labelObserver)

        super.onDestroy()
    }
}