package kr.co.citus.exampleml.util.perm

import androidx.appcompat.app.AppCompatActivity
import kr.co.citus.exampleml.util.perm.data.PermFlowControllable
import kr.or.forest.smartdaslim.util.manager.perm.struct.PermContractResult
import kr.co.citus.exampleml.util.perm.struct.PermManagerImpl
import java.util.LinkedList

/**
 * @author dspark
 * @since 2023-12-11
 *
 * 안드로이드 퍼미션들을 검증한다 ( 사용자 정의 퍼미션은 고려하지 않았다. )
 *
 * 선택 퍼미션은 물어보지만, 거절해도 onFailContract()을 타지 않지만,
 * 필수 퍼미션은 거절시 onFailContract()을 탄다.
 *
 * onCreate()에서 해당 클래스를 초기화 해야 한다.
 * onStart()이후 초기화 해서 사용하면 앱이 죽을꺼다.
 *
 * @see CommonPermUnit 선택 퍼미션 데이터 클래스
 * @see UrgentPermUnit 필수 퍼미션 데이터 클래스
 *
 * @apiNote
 * 해당 클래스는 퍼미션 검증 결과에 대한 콜백을 던지는 역활만 담당한다.
 * 실제 퍼미션을 검증하는 곳은, 모체 클래스 PermManagerImpl이 담당한다.
 *
 * 즉, PermManagerImpl이 검증 결과를 던지면, 여기서는 검증 결과에 대현 콜백을 사용자에게 던진다.
 */
class PermContractManager(activity: AppCompatActivity, private var onPermContractResultListener: PermContractResult?) : PermManagerImpl(activity) {

    /**
     * 모든 권한을 검증 했다면, 분기한다
     */
    override fun onContractAll(reqPermList: LinkedList<PermFlowControllable>) {
        this.onPermContractResultListener.let { it?.onContractAll(reqPermList) }
    }

    /**
     * 권한을 검증하지 못했다면 분기한다
     *
     * @param permName 검증받지 못한 안드로이드 퍼미션 이음
     * @param toDisplayMessage 사용자에게 안내할 문구
     */
    override fun onFailContract(permName: String, toDisplayMessage: String) {
        this.onPermContractResultListener.let { it?.onFailContract(permName, toDisplayMessage) }
    }

    /**
     * 모든 자원을 해제하고 리스너를 밀어버린다.
     */
    override fun release() {
        super.release()
        this.onPermContractResultListener = null
    }
}