package kr.co.citus.exampleml.util.converter

enum class ClassesDefine (val labelName: String, val realName: String) {
    ASTON_MARTIN("amr", "애스턴 마틴"),
    FERARRI("ferarri", "스쿠데리아 페라리"),
    MCLAREN("mcl", "멕라렌 레이싱"),
    MERCEDES("merc", "메르세데스 AMG 페트로나스"),
    OTHERS("others", "기타"),
    RBR("rbr", "오라클 레드불 레이싱"),
    VEHICLE("vehicle", "차량"),
    WILLIAMS_RACING("williams", "윌리엄스 레이싱")
}