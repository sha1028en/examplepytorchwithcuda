package kr.co.citus.exampleml.dialog.importImage

interface ImportImageDialogOnClickListener {

    // select import img from Gallery
    fun onClickAlbum()

    // select import img from camera
    fun onClickCamera()
}