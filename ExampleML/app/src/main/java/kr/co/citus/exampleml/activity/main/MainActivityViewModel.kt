package kr.co.citus.exampleml.activity.main

import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.OnCanceledListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.mlkit.common.model.LocalModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.label.ImageLabel
import com.google.mlkit.vision.label.ImageLabeling
import com.google.mlkit.vision.label.custom.CustomImageLabelerOptions

class MainActivityViewModel : ViewModel() {
    private val img by lazy { MutableLiveData<Bitmap?>() }

    fun getImg() = this.img.value

    fun setImg(img: Bitmap) {
        this.img.value?.recycle()
        this.img.value = img
    }

    fun appendImgObserver(owner: LifecycleOwner, observer: Observer<Bitmap?>) {
        this.img.observe(owner, observer)
    }

    fun dependImgObserver(observer: Observer<Bitmap?>) {
        this.img.removeObserver(observer)
    }

    private val label by lazy { MutableLiveData<String>() }

    fun getLabel() = this.label.value ?: "안녕하세요, 마티아 비노토."

    fun setLabel(label: String) {
       this.label.value = label
    }

    fun appendLabelObserver(owner: LifecycleOwner, observer: Observer<String>) {
        this.label.observe(owner, observer)
    }

    fun dependLabelObserver(observer: Observer<String>) {
        this.label.removeObserver(observer)
    }

    private val labeler by lazy {
        val formulaModel = LocalModel.Builder()
            .setAssetFilePath("FormulaModel_float32.tflite")
            .build()

        val imgLabelOptions = CustomImageLabelerOptions.Builder(formulaModel)
            .setMaxResultCount(2)
            .setConfidenceThreshold(0.5f)
            .build()

        ImageLabeling.getClient(imgLabelOptions)
    }

    fun processImageLabel(img: InputImage, onSuccessListener: OnSuccessListener<List<ImageLabel>>, onFailureListener: OnFailureListener) =
        this.labeler.process(img)
            .addOnSuccessListener(onSuccessListener)
            .addOnFailureListener(onFailureListener)

    override fun onCleared() {
        this.img.value?.recycle()
        this.labeler.close()

        super.onCleared()
    }
}